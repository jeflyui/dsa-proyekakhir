# Cara Menjalankan Server

Aplikasi Web menggunakan bahasa R dan direkomendasikan menggunakan R studio.

## Persiapkan libraries / packages
lakukan instalasi dan jalankan kode dibawah ini.

    library(tm)
    library(stringi)
    library(stringr)
    library(katadasaR)
    library(e1071)
    library(SnowballC)
    library(wordcloud)
    library(caret)
    library(klaR)
    library(cvTools)
    library(shiny)
    library("ggplot2")
    library(twitteR)

## Jalankan fungsi
Jalankan file-file R berisi fungsi yang dibutuhkan berikut ini
   
    artikanSingkatan.R
    removeAdminTweetAndRT.R
    removeStopword.R
    
## Learning Model
Proses learning model dilakukan dengan menjalankan file R (pilih salah satu)
   
    compute_NaiveBayes.R #untuk model naive bayes
    compute_SVM.R #untuk model SVM

## Jalankan Server

    runApp('dsa_commuterline')
    
    #atau dengan click button "Run App" di Rstudio